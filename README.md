# Freediving & Spearfishing Buddy Tracker

![Next to mask](iter4-mask.png)

The most dangerous aspect of freediving and spearfishing is a physiological event known as a shallow-water-blackout (SWB).

This occurs when a diver is ascending from deep depths (30+ meters). The partial pressure of oxygen on the lungs drops dangerously as the lungs double in volume in the last 10 meters, and a diver can black out underwater.

The device uses LoRa to transmit GPS coordinates peer-to-peer between dive partners, which then can use audio signals to guide divers towards one another when requested, and to give audible alarms if a partner doesn't resurface or if partners drift too far apart. 

*This project takes advantage of the fact that radio waves at high-frequency do not penetrate the water.*

I designed these GPS beacons with feedback from the Monterey Freedivers facebook group and the California Spearfishers facebook group, and am in the process of working with a larger spearfishing manufacturer. These conversations are saved to the folder "feedback". A lot of very key user input was gathered here, a few examples of user feedback:

![Feedback 1](feedback/feedback1.png)

![Feedback 2](feedback/feedback2.png)

![Feedback 3](feedback/feedback3.png)



# App prototype: Available  [here](https://www.justinmind.com/usernote/tests/48277444/48287647/48288000/index.html?fbclid=IwAR3TC2xHDnNERtAp8fhjZzU2pTsFKQMs8NmVo0RpDeYBvj_JJSOq_-rCPYo#/screens/cf2adb63-81a8-412e-a8ae-4c0d506523fa).

Rather than designing a phone app, this device hosts its own WiFi network and webserver to allow users to configure their device settings.

# Iteration 1 - Snorkel/Tube Mounted

![Snorkel Mounter GPS Beacon](BuddyTrackerWithTube.png)

![Snorkel Mounted With Enclosure](iter1-tube.png)

The community really resisted the idea of mounting the entire device to the snorkel, as it adds a lot of drag while diving, and easy to lose.

# Iteration 2 - Mask Mounted

![Mask Only](iter2-mask.png)

![Snorkel Antenna](iter2-tube.png)

# Iteration 3 - Start of testing

![PCB With Battery](pcb4.png)

![With Enclosure](pcb4-enclosure.png)

![Mounted](pcb4-mounted.png)



